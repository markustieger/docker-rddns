FROM rust:1.69.0 AS builder
LABEL maintainer="markustieger@gmail.com"

ARG TARGET_PLATFORM=x86_64-unknown-linux-musl

RUN apt-get update && \
    apt-get install --no-install-recommends -y musl-tools && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN rustup target add ${TARGET_PLATFORM}

# copy source
COPY . /work
WORKDIR /work

# build
RUN cargo build --release --target=${TARGET_PLATFORM}

FROM scratch
LABEL maintainer="markustieger@gmail.com"

ARG TARGET_PLATFORM=x86_64-unknown-linux-musl

# install
COPY --from=builder /work/target/${TARGET_PLATFORM}/release/docker-rddns /docker-rddns

ENTRYPOINT ["/docker-rddns"]
