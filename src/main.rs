
use std::collections::HashMap;
use std::env;
use axum::http::{StatusCode};
use axum::{Json, Router};
use axum::routing::{post};

use bollard::models::{EndpointIpamConfig, Ipam};
use bollard::network::{ConnectNetworkOptions, CreateNetworkOptions, DisconnectNetworkOptions, InspectNetworkOptions, ListNetworksOptions};
use bollard::service::{EndpointSettings};
use bollard::Docker;
use lazy_static::lazy_static;
use serde::Deserialize;
use tokio::sync::Mutex;
use regex::Regex;

lazy_static!(
    static ref LAST_CHECK: Mutex<Option<UpdatePayload>> = Mutex::new(None);
    static ref DOCKER: Mutex<Docker> = Mutex::new(Docker::connect_with_unix_defaults().expect("Failed to connect to docker!"));
);

#[tokio::main]
async fn main() {
    let bind = env::var("SERVER_BIND_ADDRESS").unwrap_or(String::from("[::]:49823"));
    println!("Starting docker-rddns on {}", bind.clone());

    let app = Router::new()
        .route("/", post(update));

    let listener = tokio::net::TcpListener::bind(bind.as_str()).await.unwrap();
    axum::serve(listener, app).await.unwrap();

}

#[derive(Deserialize, Debug, PartialEq, Clone)]
pub struct UpdatePayload {
    name: String,
    network: Option<NetworkConfig>,
    containers: HashMap<String, ContainerConfig>,
}

#[derive(Deserialize, Debug, Default, PartialEq, Clone)]
pub struct ContainerConfig {
    ip4: Option<String>,
    ip6: Option<String>
}

#[derive(Deserialize, Debug, Default, PartialEq, Clone)]
pub struct NetworkConfig {
    ipam: Option<Ipam>,
    driver: Option<String>,
    options: HashMap<String, String>
}

async fn update(Json(data): Json<UpdatePayload>) -> Result<String, StatusCode> {

    let option = Some(data.clone());
    if LAST_CHECK.lock().await.eq(&option) {
        println!("Nothing to update!");
        return Ok(String::new());
    }
    
    println!("Update got triggered! {:?}", data);

    let docker = DOCKER.lock().await;
    let result = docker.list_networks(Some(ListNetworksOptions {
        filters: HashMap::from([
            (String::from("name"), vec![data.name.clone()])
        ])
    })).await;

    let networks = result.map_err(|err| {
        println!("{}", err);
        StatusCode::INTERNAL_SERVER_ERROR
    })?;

    match networks.get(0) {
        None => {},
        Some(network) => {
            let default_scope = String::from("local");
            let network = docker.inspect_network(data.name.as_str(), Some(InspectNetworkOptions {
                verbose: true,
                scope: network.scope.as_ref().unwrap_or(&default_scope).clone()
            })).await.map_err(|err| {
                println!("{}", err);
                StatusCode::INTERNAL_SERVER_ERROR
            })?;

            let empty_map = HashMap::new();
            for (_key, container) in network.containers.as_ref().unwrap_or(&empty_map) {
                docker.disconnect_network(data.name.as_str(), DisconnectNetworkOptions {
                    container: container.name.as_ref().unwrap().clone(),
                    force: true
                }).await.map_err(|err| {
                    println!("{}", err);
                    StatusCode::INTERNAL_SERVER_ERROR
                })?;
            }

            docker.remove_network(data.name.as_str()).await.map_err(|err| {
                println!("{}", err);
                StatusCode::INTERNAL_SERVER_ERROR
            })?;
        }
    }

    let net_default = NetworkConfig::default();

    let mut net = CreateNetworkOptions::default();
    let network_config = data.network.as_ref().unwrap_or(&net_default).clone();
    net.ipam = network_config.ipam.clone().unwrap_or(Ipam::default());
    net.driver = network_config.driver.clone().unwrap_or(String::from("bridge"));
    net.options = network_config.options.clone();
    for (_key, value) in &mut net.options {
        if value.starts_with("[IF:") && value.ends_with("]") {
            let regex = &value[4..(value.len()-1)];
            let regex = Regex::new(regex).unwrap();
            *value = pnet::datalink::interfaces()
                .into_iter()
                .filter(|iface| regex.is_match(&iface.name))
                .next().unwrap().name;
        }
    }
    net.name = data.name.clone();
    net.enable_ipv6 = true;

    println!("Creating Network: {:?}", net);

    docker.create_network(net).await.map_err(|err| {
        println!("{}", err);
        StatusCode::INTERNAL_SERVER_ERROR
    })?;

    for (container, container_data) in &data.containers {
        let mut endpoint_config = EndpointSettings::default();
        let mut ipam_config = EndpointIpamConfig::default();
        

        ipam_config.ipv4_address = container_data.ip4.clone();
        ipam_config.ipv6_address = container_data.ip6.clone();

        endpoint_config.ipam_config = Some(ipam_config);

        docker.connect_network(data.name.as_str(), ConnectNetworkOptions {
            container: container.clone(),
            endpoint_config
        }).await.map_err(|err| {
            println!("{}", err);
            StatusCode::INTERNAL_SERVER_ERROR
        })?;
    }

    for (container, _) in &data.containers {
        let _ = docker.start_container::<String>(container, None).await;
    }

    println!("Update was successful!");

    *LAST_CHECK.lock().await = option;

    Ok(String::new())
}
